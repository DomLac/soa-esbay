from flask import Flask, make_response, request, json, jsonify
import smtplib

app = Flask(__name__)


@app.route('/email/hello')
def hello():
    return 'Hello, welcome to the ESBay Email API\n'


@app.route('/api/email/sendEmailConfirmation', methods=['POST'])
def sendEmail():
    gmail_user = 'domsoa.esbay@gmail.com'
    gmail_pwd = '' # Password kept for myself

    to = request.form['to']
    productID = request.form['productID']
    productName = request.form['productName']
    user = request.form['user']
    orderID = request.form['orderID']

    smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(gmail_user, gmail_pwd)

    header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + \
        'Subject:Order number ' + orderID + ' - Reception confirmed\n'
    msg = header + '\nReception of the ' + productName + \
        ' confirmed by the user ' + user + '.\nReference of the order:' + orderID
    smtpserver.sendmail(gmail_user, to, msg)

    smtpserver.close()

    response = jsonify({'message': 'Reception confirmed',
                        'from': gmail_user, 'to': to, 'msg': msg})

    return response


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
