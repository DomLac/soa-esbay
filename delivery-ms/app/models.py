from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine

db = SQLAlchemy()


def init_app(app):
    db.app = app
    db.init_app(app)
    return db


def create_tables(app):
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    db.metadata.create_all(engine)
    return engine


class Delivery(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product = db.Column(db.String(255), nullable=False)
    buyer = db.Column(db.String(255), nullable=False)
    destination_address = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(255), nullable=False)

    def to_json(self):
        return {
            'id' : self.id,
            'product': self.product,
            'buyer': self.buyer,
            'destination_address': self.destination_address,
            'status': self.status,
        }
