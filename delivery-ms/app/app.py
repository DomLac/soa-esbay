from flask import Flask, make_response, request, json, jsonify
import models

app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY = "This is an INSECURE secret!! DO NOT use this in production!",
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqlconnector://root:test@user_db/user',
    SQLALCHEMY_TRACK_MODIFICATIONS = False,
))

models.init_app(app)
models.create_tables(app)


@app.route('/delivery/hello')
def hello():
    return 'Hello, welcome to the ESBay Delivery API\n' 


@app.route('/api/delivery/create', methods=['POST'])
def post_create():

    product = request.form['product']
    buyer = request.form['buyer']
    destination_address = request.form['destination_address']

    item = models.Delivery()
    item.product = product
    item.buyer = buyer
    item.destination_address = destination_address
    item.status = "To be posted"

    models.db.session.add(item)
    models.db.session.commit()

    response = jsonify({'message': 'Delivery added', 'delivery': item.to_json()})

    return response


@app.route('/api/delivery', methods=['GET'])
def delivery():
    data = []
    
    for row in models.Delivery.query.all():
        data.append(row.to_json())

    response = jsonify({
        'results': data
    })

    return response


@app.route('/api/deliveryGetById', methods=['GET'])
def deliveryGetById():
    id = request.args.get('id')

    delivery = models.Delivery.query.get(id)
    response = jsonify({
            'results': delivery.to_json()
        })

    return response


@app.route('/api/deliveryFilterByBuyer', methods=['GET'])
def deliveryFilterByBuyer():
    data = []
    buyerDelivery = request.args.get('buyer')

    for row in models.Delivery.query.filter_by(buyer = buyerDelivery):
        data.append(row.to_json())

    response = jsonify({
            'results': data
        })

    return response


@app.route('/api/deliveryFilterByDestinationAddress', methods=['GET'])
def deliveryFilterByDestinationAddress():
    data = []
    addressDelivery = request.args.get('address')

    for row in models.Delivery.query.filter_by(destination_address = addressDelivery):
        data.append(row.to_json())

    response = jsonify({
            'results': data
        })

    return response


@app.route('/api/deliveryFilterByProduct', methods=['GET'])
def deliveryFilterByProduct():
    data = []
    productDelivery = request.args.get('product')

    for row in models.Delivery.query.filter_by(product = productDelivery):
        data.append(row.to_json())

    response = jsonify({
            'results': data
        })

    return response


@app.route('/api/deliveryFilterByStatus', methods=['GET'])
def deliveryFilterByStatus():
    data = []
    statusDelivery = request.args.get('status')

    for row in models.Delivery.query.filter_by(status = statusDelivery):
        data.append(row.to_json())

    response = jsonify({
            'results': data
        })

    return response


@app.route('/api/deliverySetByProduct', methods=['GET'])
def deliverySetByProduct():
    data = []
    productDelivery = request.args.get('product')
    statusDelivery = request.args.get('status')

    for row in models.Delivery.query.filter_by(product = productDelivery):
        models.Delivery.query.filter_by(product = productDelivery).update({"status": statusDelivery}, synchronize_session='evaluate')
        models.db.session.commit()
        data.append(row.to_json())


    response = jsonify({
            'results': data
        })

    return response


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
